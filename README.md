# scpe_matrix_parser 

This script is done in order to transform the n*n contact matrix (outfile from SCPE) into a data frame. 
The data frame generated consists in several columns namely: 
- Archivo (str): the name of the PDB. 
- Real_Pos (int): saves the value of the position corrected (position 0 of SCPE --> position 1 of the protein structure)
- Second_pos (int) : is the second position with which is going to be evaluated if the position in Real_Pos is making contact or not. 
- Contact (int): store the information if the to positions are in contact (value: 1 ) or not (value: 0)


## Variables defined by the user
There a list of variables that must be defined by the user:
- path_matrices: is the path to the folder where the matrices are.
- name_of_matrix : in the case of parsing only one matrix
- outfile_name: is the name of the outfile where the processed data is going to be stored.

## Posible variants

As you may notice this program is donde to parse one matrix at a time, but can be easily adapated to read all matrix in the folder, or taking the matrix as an argument. 


