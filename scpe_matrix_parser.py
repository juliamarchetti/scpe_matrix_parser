import os 
import glob
import re
import pandas as pd


if __name__ == "__main__":
    path_matrices = "/home/julia/Escritorio/Matrices_prueba/"
    name_of_matrix = "Contact-1A9B-1_A.dat"
    outfile_name = "matriz_parseada.csv"
    os.chdir(path_matrices)

    #Puede ser fácilmente adaptable a leer de una lista de matrices
    i= name_of_matrix
    with open(outfile_name,"w") as file:
        file.write("Archivo,Real_Pos,Second_Pos,Contact,\n")
        with open(i,"r") as contact_file:
            lineas=contact_file.read()
            archivo=(i[8:-4])
            START="Terciary \n"
            END="Quaternary \n"
            m = re.compile(r'%s.*?%s' % (START,END), re.S)
            info_contactos = re.findall(m,lineas)
            #Le pongo 1: porque así como está me lee la linea de contactos terciarios y el -2 para que elimine la linea de quaternary
            info_contactos_lineas= info_contactos[0].split("\n")[1:-2]
            #EN este array voy a guardar los contactos corregidos, sin espacios
            contactos_corregidos=[]
            for i in range(len(info_contactos_lineas)):
                contactos_corregidos.append(info_contactos_lineas[i].strip().replace(" ", ""))#Le saco los espacios
            #saco la cantidad de posiciones totales para ir recorriendo cada fila
            total_positions=len(contactos_corregidos)
            print("La proteina tiene %d posiciones" % total_positions)
            for j in range(total_positions):
                real_pos=j+1
                for z in range(len(contactos_corregidos[j])):
                    second_pos=z+1
                    contacto_info = contactos_corregidos[j][z]
                    file.write(archivo+","+str(real_pos)+","+str(second_pos)+","+str(contacto_info)+"\n")                
